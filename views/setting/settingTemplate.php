<style>
    body:not(.page-id-833) .grecaptcha-badge {
      z-index: 99999;
    }
</style>

<script src="https://www.google.com/recaptcha/api.js?render=6Lc_YsoaAAAAADJ1mWRWeRU3B53cR8lcaho-5IZI"></script>
<script>
grecaptcha.ready(function() {
    grecaptcha.execute('6Lc_YsoaAAAAADJ1mWRWeRU3B53cR8lcaho-5IZI', {action: 'homepage'}).then(function(token) {
    });
});
</script>

<?php include("../../auth.php"); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Textinzerce.cz - Inzeráty, nabídky prací, prodeje, nákupy, vše na jednom místě! - Textinzerce.cz</title>
    <meta name="description" content="Inzerce zdarma, internetový textový bazar - kupte si nový mobil nebo nabízejte práci, to vše zvládne Textinzerce.cz - Textové inzeráty.">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Jan Možný">
    <link rel="stylesheet" href="css/bootstrap/bootstrap.min.css">
    <link rel="icon" href="favicon.ico">
  </head>
  <body>

<section>
  <nav class="navbar">
    <div class="container">
      <a class="navbar-brand" href="https://textinzerce.cz/"><button type="button" class="btn btn-outline-primary mr-2">🚀 Textinzerce.cz</button></a>
      <div class="ml-auto"><a class="btn btn-outline-primary mr-2" href="/views/profile/profileTemplate.php">Osobní účet</a></div>
    </div>
  </nav>
</section>
    
<section class="py-5">
  <div class="container text-center">
    <h2 class="display-4">99,-<small class="text-muted">/ VIP inzerát</small></h2>
    <p class="lead mb-4">Inzerujte v kategoriích jako první!</p>
    <a class="btn btn-lg btn-primary" href="https://form.simpleshop.cz/rbzj/">Koupit VIP</a>
  </div>
</section>
      
<hr style="width:50%">
<div class="container">
	<div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
		<h1 class="display-4">Nastavení</h1>
	</div>
<div class="container"> 
<?php
    function functionSelectDataSetting() {
        include("../../auth.php");
        include('../../dbConnection/dbConnection.php');
        $dbConnection = new dbConnection();
        $login_user = $_SESSION['username'];
        $sql = "SELECT * FROM userinfo
        LEFT JOIN users ON users.id = userinfo.user_id
        WHERE users.username = '$login_user'";
        $result = mysqli_query($dbConnection->connectDB(), $sql);
        if (mysqli_num_rows($result) > 0) {
            while($row = mysqli_fetch_assoc($result)) {
                $firstname = $row["firstname"];
                $GLOBALS["lastname"] = $row["lastname"];
                $GLOBALS["phone"] = $row["phone"];
                $GLOBALS["street"] = $row["street"];
                $GLOBALS["email"] = $row["email"];
                $GLOBALS["city"] = $row["city"];
                $GLOBALS["secret"] = $row["secret"];
                $GLOBALS["gender"] = $row["gender"];
            }
        } else {
            echo "Zero results";
        }
        mysqli_close($dbConnection->connectDB());
    }
    functionSelectDataSetting();
?>
<div class="container bootstrap snippet">
    <div class="row">
        <div class="col-sm-9 mx-auto">
            <div class="tab-content">
                <div class="tab-pane active" id="home">
                    <form class="form" action="../../services/usersInfoServices.php" method="post" id="registrationForm">
                        <div style="display: none";>
                            <label for="last_name"><h4>Jméno</h4></label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">✎</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Jméno" aria-label="Username" aria-describedby="basic-addon1" name="username">
                            </div>
                        </div>
                        <label for="last_name"><h4>Jméno a příjmení</h4></label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">➦</span>
                            </div>
                            <input type="text" class="form-control" placeholder="Jméno a příjmení" aria-label="Username" aria-describedby="basic-addon1" name="lastname" value="<?php echo $lastname ?>" required>
                        </div>
                        <label for="last_name"><h4>Telefon</h4></label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">➦</span>
                            </div>
                            <input type="tel" class="form-control" placeholder="Telefon" aria-label="Username" aria-describedby="basic-addon1" name="phone" value="<?php echo $phone ?>" required>
                        </div>
                        <label for="last_name"><h4>Email</h4></label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">➦</span>
                            </div>
                            <input type="text" class="form-control" placeholder="Email" aria-label="Username" aria-describedby="basic-addon1" name="email" value="<?php echo $email ?>" required>
                        </div>
                        <label for="last_name"><h4>Město</h4></label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">➦</span>
                            </div>
                            <input type="text" class="form-control" placeholder="Město" aria-label="Username" aria-describedby="basic-addon1" name="city" value="<?php echo $city ?>" required>
                        </div>
                        <label for="last_name"><h4>Ulice, č. popisné a PSČ</h4></label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">➦</span>
                            </div>
                            <input type="text" class="form-control" placeholder="Ulice a č. popisné" aria-label="Username" aria-describedby="basic-addon1" name="street" value="<?php echo $street ?>" required>
                        </div>
                        <label for="last_name"><h4>Zakoupený kód</h4></label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">➦</span>
                            </div>
                            <input type="text" class="form-control" placeholder="Koupený kód" aria-label="Username" aria-describedby="basic-addon1" name="secret" value="<?php echo $secret ?>" required>
                        </div>
						<p style="margin-bottom: -10px; margin-top: 25px"><sup><em>Odstranit účet: <a href='/views/sendingTemplate.php'>Odstranit</a></em></sup></p>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <br>
                                <button class="btn btn-lg btn-primary" type="submit" value="submit">
                                    Uložit
                                </button>
                                <a href="../index.php">
                                    <button type="button" class="btn btn-danger" style="margin-left: 5px; cursor: pointer">Zpátky</button>
                                </a>
                            </div>
                            <br>
                        </div>
                    </form>
                    <hr>
                    <form action="../../services/changeUsersPasswordServices.php" method="POST">
              				<div class="form-group">
              					<h4 for="heslo" style="margin-bottom:15px">Změnit heslo</h4>
              					<input id="heslo" style="display: block;" type="password" class="form-control" name="newPassword" placeholder="Změnit heslo" required>
              				</div>
              				<div class="card-action">
              					<button class="btn btn-lg btn-primary">Uložit</button>
              				</div>
        		        </form>
        		      <hr>
        		 </div>
            </div>
        </div>
    </div>
</div>

<section class="py-5">
  <div class="container">
    <div class="row mb-5">
      <div class="col-12 col-lg-2 mb-4 mb-lg-0 text-center text-lg-left"></div>
      <div class="col-12 col-lg-4 mb-5 mb-lg-0 text-center text-lg-left">
        <p class="text-muted fs-6 mb-0">Snažíme se přinášet novinky každý den. Pokud chcete být informováni, přihlaste se k newsletteru</p>
      </div>
      <div class="col-12 col-lg-3 mb-4 mb-lg-0 text-center text-lg-left">
        <h6 class="fw-bold">Support</h6>
        <p class="fs-6 text-muted mb-0">support@textinzerce.cz</p>
      </div>
      <div class="col-12 col-lg-3 text-center text-lg-left">
        <h6 class="fw-bold">Platby</h6>
        <p class="fs-6 text-muted mb-0">payinfo@textinzerce.cz</p>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-lg-6 text-center text-lg-left order-1 order-lg-0">
        <p class="small text-muted">Copyright &copy; 2021, Textinzerce.cz</p>
      </div>
      <div class="col-12 col-lg-6 mb-4 mb-lg-0 text-center text-lg-right"><a style="text-decoration: none;" class="mr-3" href="../terms/termTemplate.php">Podmínky používání</a></div>
    </div>
  </div>
</section>
</div>
</body>
</html>