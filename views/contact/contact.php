<?php
$from = 'uloztomedik.cz<honza.mozny@seznam.cz>';
$sendTo = 'honza.mozny@seznam.cz';
$subject = 'Nová zpráva z uloztomedik.cz';
$fields = array('name' => 'Jméno', 'surname' => 'Příjmení', 'email' => 'Email', 'message' => 'Zpráva');
$okMessage = '<script>alert("Zpráva byla odeslána. Děkujeme, budeme vás co nejdříve kontaktovat."); location.replace(document.referrer);</script>';
$errorMessage = 'Při odesílání zprávy došlo k chybě. Prosím zkuste to znovu';
error_reporting(E_ALL & ~E_NOTICE);
try
{
    if(count($_POST) == 0) throw new \Exception('Formulář je prázdný');
    $emailText = "Máte novou zprávu od\n=============================\n";
    foreach ($_POST as $key => $value) {
        if (isset($fields[$key])) {
            $emailText .= "$fields[$key]: $value\n";
        }
    }
    $headers = array('Content-Type: text/plain; charset="UTF-8";',
        'From: ' . $from,
        'Reply-To: ' . $from,
        'Return-Path: ' . $from,
    );
    mb_send_mail($sendTo, $subject, $emailText, implode("\n", $headers));
    $responseArray = array('type' => 'success', 'message' => $okMessage);
}
catch (\Exception $e)
{
    $responseArray = array('type' => 'danger', 'message' => $errorMessage);
}
if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    $encoded = json_encode($responseArray);
    header('Content-Type: application/json');
    echo $encoded;
}
else {
    echo $responseArray['message'];
}