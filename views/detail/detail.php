<script src="https://www.google.com/recaptcha/api.js?render=6Lc_YsoaAAAAADJ1mWRWeRU3B53cR8lcaho-5IZI"></script>
<script>
grecaptcha.ready(function() {
    grecaptcha.execute('6Lc_YsoaAAAAADJ1mWRWeRU3B53cR8lcaho-5IZI', {action: 'homepage'}).then(function(token) {
    });
});
</script>

<style>
  .zoom {
      transition: transform .2s; 
    }
    
   .zoom:hover {
      transform: scale(1.1);
    }
    
    body:not(.page-id-833) .grecaptcha-badge {
      z-index: 99999;
    }
</style>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Textinzerce.cz - Inzeráty, nabídky prací, prodeje, nákupy, vše na jednom místě! - Textinzerce.cz</title>
    <meta name="description" content="Inzerce zdarma, internetový textový bazar - kupte si nový mobil nebo nabízejte práci, to vše zvládne Textinzerce.cz - Textové inzeráty.">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Jan Možný">
    <link rel="stylesheet" href="css/bootstrap/bootstrap.min.css">
    <link rel="icon" href="favicon.ico">
  </head>
  <body>
      <section>
        <nav class="position-relative navbar navbar-expand-lg navbar-light">
          <div class="container">
            <a class="navbar-brand" href="https://textinzerce.cz"><button type="button" class="btn btn-outline-primary mr-2">🚀 Textinzerce.cz</button></a>
            <button class="navbar-toggler" type="button" data-toggle="side-menu" data-target="#sideMenu04" aria-controls="sideMenu04" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse">
              <ul class="navbar-nav position-absolute top-50 left-50 translate-middle">
                <li class="nav-item mr-4"><a class="nav-link" href="https://textinzerce.cz#newsletter">Novinky</a></li>
                <li class="nav-item mr-4"><a class="nav-link" href="/views/profile/profileTemplate.php">Inzeruj</a></li>
                <li class="nav-item mr-4"><a class="nav-link" href="/views/profile/profileTemplate.php">VIP inzeráty</a></li>
                <li class="nav-item mr-4"><a class="nav-link" href="https://textinzerce.cz/#message">Napište nám</a></li>
              </ul>
              <div class="ml-auto"><a class="btn btn-primary" href="/views/profile/profileTemplate.php">Uživatelský účet</a></div>
            </div>
          </div>
        </nav>
        <div class="d-none fixed-top top-0 bottom-0" id="sideMenu04" style="z-index:999999">
          <div class="position-absolute top-0 right-0 bottom-0 left-0 bg-dark" style="opacity: 0.7"></div>
          <nav class="navbar navbar-light position-absolute top-0 bottom-0 left-0 w-75 pt-3 pb-4 px-4 bg-white" style="overflow-y: auto;">
            <div class="d-flex flex-column w-100 h-100">
              <div class="d-flex justify-content-between">
                <a style="color: #0d6efd" class="navbar-brand" href="https://textinzerce.cz">🚀 Textinzerce.cz</a>
                <button class="btn-close" type="button" data-toggle="side-menu" data-target="#sideMenu04" aria-controls="sideMenu04" aria-label="Close"></button>
              </div>
              <div>
                <ul class="navbar-nav mb-4">
                  <li class="nav-item"><a class="nav-link" href="https://textinzerce.cz#newsletter">Novinky</a></li>
                  <li class="nav-item"><a class="nav-link" href="/views/profile/profileTemplate.php">Inzeruj</a></li>
                  <li class="nav-item"><a class="nav-link" href="/views/profile/profileTemplate.php">VIP inzeráty</a></li>
                  <li class="nav-item"><a class="nav-link" href="https://textinzerce.cz/#message">Napište nám</a></li>
                </ul>
                <div class="ml-auto"><a class="btn btn-outline-primary mr-2" href="/views/profile/profileTemplate.php">Osobní účet</a></div>
              </div>
              <div class="mt-auto">
                <p>
                  <span>Napište nám: </span>
                  <a href="mailto:support@textinzerce.cz">support@textinzerce.cz</a>
                </p>
            </div>
          </nav>
        </div>
      </section>
    
      <section class="position-relative py-5 bg-primary" style="overflow: hidden; z-index: 1;">
        <div class="container">
          <div class="row">
            <div class="col-12 col-md-8 col-lg-6 mx-auto mb-4 text-center text-lg-left">
              <h2 class="mb-3 fs-1 text-white fw-bold">
                <span>Prodávejte</span>
                <span class="text-warning">/ nakupujte.</span>
                <span><br>Vše na jednom místě.</span>
              </h2>
              <p class="text-white-50 mb-0">Pomáháme inzerci růst.</p>
            </div>
            <div class="col-12 col-lg-6 d-flex flex-column justify-content-center">
              <div class="row justify-content-center justify-content-lg-end">
              <form action='/views/search/searchAd.php' method='GET'>
                <div  style="display: flex; width: auto;" class="col-12 col-md-6 col-lg-8 mb-3 mb-md-0">
                  <div class="input-group">
                    <input style="width:auto;" name="searchAd" class="form-control" type="text" placeholder="Napiště název inzerátu">
                  </div>
                  <div style="width:auto;" class="col-12 col-md-auto">
                    <button class="w-100 btn btn-light" type="submit">Hledat</button>
                  </div>
                </div>
                </form>
              </div>
            </div>
          </div>
          <img class="d-none d-lg-block position-absolute img-fluid top-0 left-0" style="margin-top: -250px; margin-left: -150px; z-index: -5;" src="metis-assets/elements/square-rotated.svg" alt="">
        </div>
      </section>
    
    <div class="container bootstrap snippet">
    <div class="card ">
      <div class="card-header">
      <a onclick="getURL()" href="#" style="font-family: arial; text-decoration:none; color:#0d6efd; margin-bottom:10px; border: 2px solid #0d6efd; padding: 10px; border-radius: 3px; display: inline-flex; margin-top: 10px;"> &nbspSdílet nabídku</a>
        <script>
            function getURL() {
            prompt("Sdílejte tímto odkazem", window.location.href);
        }
        </script>
      </div>
    	  <div class="card-body">
    		<blockquote class="blockquote mb-0">
            <?php
            function plain( $str ) {
            	return htmlspecialchars( $str, ENT_QUOTES );
            }
            ?>
            <div class="container bootstrap snippet">
            <div class="row"> 
            <?php 
            function getProductToDetailPage() {
            	require('../../dbConnection/dbConnection.php');
            	$dbConnection = new dbConnection();
            	$query = mysqli_query($dbConnection->connectDB(),"SELECT * FROM `postData` WHERE id=".($_GET['id'])." LIMIT 1");
            	$result = mysqli_query($dbConnection->connectDB(), $query);
            	$count = mysqli_num_rows($query);
            		if ($count == "0") {
            			echo '<h2 style="text-align:center;">Výsledky nebyly nalezeny</h2>';
            		} else {
            			while($row = mysqli_fetch_array($query)) {
            				$id = $row["id"];
            				$image = $row["image"];
            				$title = $row["title"];
            				$article = $row["article"];
            				$contact = $row["contact"];
            				$price = $row["price"];
            				if($row["vip_kod"]) {
            				$top = "🆙";
            				echo "<div class='embed-responsive col-md-3' style='z-index:9999; width: 100%'>";
            					echo "<div class='card d-inline-flex p-2' style='width: 18rem; margin-top: 0px; width: 50%; float:right'>";
            						echo "<div class='card-body'>";
            							echo "<h5 class='card-title'>" . $title . "<hr></h5>";
            							echo "<p class='card-text' style='margin-bottom: 0px'>" . $article . "<hr></p>";
            							echo "<h6 class='card-text' style='font-weight:700; margin-bottom: 0px'>" . $contact . "<hr></h6>";
            							echo "<h6 class='card-text' style='font-weight:700; margin-bottom: 0px'>Cena: " . $price . "</h6>";
            						echo "</div>";
            				    echo "</div>";
            				    echo "<div class='embed-responsive col-md-3' style='z-index:9999; width: 100%'>";
            					    echo "<div class='card d-inline-flex p-2' style='width: 18rem; margin-top: 0px; width: 50%; float:left'>";
                					    echo "<div style='text-align:left; z-index:99999; margin-bottom:-45px; margin-left:0px; font-size:32px'>";
                					        echo $top;
                					    echo "</div>";
            						    echo "<div class='card-body'>";
            							    echo "<a href='/services/images/".$image."' class='d-inline-flex p-2'>
            							        <img src='/services/images/".$image."' title='image' style='width:100%; height:100%; display:block; margin-left:-20px; margin-top:-20px;'></a><br>";
                                            echo "</a>";
                						echo "</div>";
                					echo "</div>";
                				echo "</div>";
            				echo "</div>";
            				} else {
            				   echo "";
            				}
            			}
            		}
            	}
            echo getProductToDetailPage();
            ?>
    		</blockquote>
                <hr style="padding-bottom: 1px; margin-bottom: 25px">
                 <div id="HCB_comment_box" style="margin-top: -15px"><a href="http://www.htmlcommentbox.com">Widget</a> načítání komentářů...</div>
                 <script type="text/javascript" id="hcb">
                    if(!window.hcb_user){hcb_user={};} (function(){var s=document.createElement("script"), l=hcb_user.PAGE || (""+window.location).replace(/'/g,"%27"), h="https://www.htmlcommentbox.com";s.setAttribute("type","text/javascript");s.setAttribute("src", h+"/jread?page="+encodeURIComponent(l).replace("+","%2B")+"&mod=%241%24wq1rdBcg%24J39ycz7i%2FeeuNkvqt8jOh%2F"+"&opts=16862&num=10&ts=1574859151790");if (typeof s!="undefined") document.getElementsByTagName("head")[0].appendChild(s);})(); 
                </script>
                <script>
                hcb_user = {
                    comments_header : 'Komentáře',
                    name_label : 'Jméno',
                    content_label: 'Váš komentář',
                    submit : 'Přidat komentář',
                    logout_link : '<img title="log out" src="https://www.htmlcommentbox.com/static/images/door_out.png" alt="[logout]" class="hcb-icon hcb-door-out"/>',
                    admin_link : '<img src="https://www.htmlcommentbox.com/static/images/door_in.png" alt="[login]" class="hcb-icon hcb-door-in"/>',
                    no_comments_msg: 'Zatím nikdo nekomentoval. Buďte první!<br>',
                    add:'Přidejte svůj komentář',
                    again: 'Přidejte svůj komentář',
                    rss:'<img src="https://www.htmlcommentbox.com/static/images/feed.png" class="hcb-icon" style="display:none" alt="rss"/> ',
                    said:'odeslal:',
                    prev_page:'<img src="https://www.htmlcommentbox.com/static/images/arrow_left.png" class="hcb-icon" title="Předchozí" alt="[prev]"/>',
                    next_page:'<img src="https://www.htmlcommentbox.com/static/images/arrow_right.png" class="hcb-icon" title="Další" alt="[next]"/>',
                    showing:'Ukázat',
                    to:'do',
                    website_label:'web (volitelné)',
                    email_label:'email',
                    anonymous:'Anonym',
                    mod_label:'(mod)',
                    subscribe:'Napsat odpověď',
                    add_image:'Přidat obrázek',
                    are_you_sure:'Chcete označit tento komentář jako nevhodný?',
                
                    reply:'<img src="https://www.htmlcommentbox.com/static/images/reply.png"/> komentovat',
                    flag:'<img src="https://www.htmlcommentbox.com/static/images/flag.png"/> nahlásit',
                    like:'<img src="https://www.htmlcommentbox.com/static/images/like.png"/> líbí',
                
                    /* dates */
                    days_ago:'dny před',
                    hours_ago:'hodiny před',
                    minutes_ago:'minuty před',
                    within_the_last_minute:'během poslední minuty',
                
                    msg_thankyou:'Děkujeme Vám za komentář.',
                    msg_approval:'(tento komentář nebude zveřejněn, dokud nebude schválen)',
                    msg_approval_required:'Děkujeme za komentář! Váš komentář se objeví, jakmile jej moderátor schválí.',
                
                    err_bad_html:'Váš komentář obsahoval html kod.',
                    err_bad_email:'Prosím zadejte platnou emailovou adresu.',
                    err_too_frequent:'Mezi zveřejněním komentářů musíte počkat několik sekund.',
                    err_comment_empty:'Váš komentář nebyl zveřejněn, protože byl prázdný!',
                    err_denied:'Váš komentář nebyl přijat.',
                    err_unknown:'Váš komentář byl zablokován z neznámých důvodů. Nahlaste to prosím.',
                    err_spam:'Váš komentář byl detekován jako spam.',
                    err_blocked:'Váš komentář byl zablokován zásadami webu.',
                
                    MAX_CHARS: 8192,
                    PAGE:'', 
                    ON_COMMENT: function(){}, 
                    RELATIVE_DATES:true
                };
                </script>
    	        </div>
        	</div>
        </div>
    </div>
    
      <section class="py-5">
        <div class="container">
          <div class="row mb-5">
            <div class="col-12 col-lg-2 mb-4 mb-lg-0 text-center text-lg-left"></div>
            <div class="col-12 col-lg-4 mb-5 mb-lg-0 text-center text-lg-left">
              <p class="text-muted fs-6 mb-0">Snažíme se přinášet novinky každý den. Pokud chcete být informováni, přihlaste se k newsletteru</p>
            </div>
            <div class="col-12 col-lg-3 mb-4 mb-lg-0 text-center text-lg-left">
              <h6 class="fw-bold">Support</h6>
              <p class="fs-6 text-muted mb-0">support@textinzerce.cz</p>
            </div>
            <div class="col-12 col-lg-3 text-center text-lg-left">
              <h6 class="fw-bold">Platby</h6>
              <p class="fs-6 text-muted mb-0">payinfo@textinzerce.cz</p>
            </div>
          </div>
          <div class="row">
            <div class="col-12 col-lg-6 text-center text-lg-left order-1 order-lg-0">
              <p class="small text-muted">Copyright &copy; 2021, Textinzerce.cz</p>
            </div>
            <div class="col-12 col-lg-6 mb-4 mb-lg-0 text-center text-lg-right"><a style="text-decoration: none;" class="mr-3" href="../terms/termTemplate.php">Podmínky používání</a></div>
          </div>
        </div>
      </section>
      </div>

<script src="js/bootstrap/bootstrap.bundle.min.js"></script>
<script src="js/main.js"></script>
</body>
</html>