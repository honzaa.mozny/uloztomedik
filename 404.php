<style>
    body:not(.page-id-833) .grecaptcha-badge {
      z-index: 99999;
    }
</style>

<script src="https://www.google.com/recaptcha/api.js?render=6Lc_YsoaAAAAADJ1mWRWeRU3B53cR8lcaho-5IZI"></script>
<script>
grecaptcha.ready(function() {
    grecaptcha.execute('6Lc_YsoaAAAAADJ1mWRWeRU3B53cR8lcaho-5IZI', {action: 'homepage'}).then(function(token) {
    });
});
</script>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Textinzerce.cz</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet" href="/assets/css/bootstrap/bootstrap.min.css">
    <link rel="icon" href="favicon.ico">
  </head>
  <body>
    <div class="">
    
      <section class="py-5">
        <div class="container text-center">
          <div class="row mb-5">
            <div class="col-12 col-md-8 col-lg-5 mx-auto"><img class="img-fluid" src="/assets/metis-assets/illustrations/error1.png" alt=""></div>
          </div>
        </div>
      </section>
    </div>
    <script src="/js/bootstrap/bootstrap.bundle.min.js"></script>
    <script src="/js/main.js"></script>
  </body>
</html>
