<?php
require ('../auth.php');
require ('../dbConnection/dbConnection.php');
class usersInfoSelectServices {
    private $firstname;
    private $lastname;
    private $phone;
    private $email;
    private $city;
    private $street;
    private $secret;
    private $profileinfo;
    function __construct() {
        $this->firstname;
        $this->lastname;
        $this->phone;
        $this->email;
        $this->city;
        $this->street;
        $this->secret;
        $this->profileinfo;
    }
    function getFirstname()
    {
        return $this->firstname;
    }
    function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }
    function getLastname()
    {
        return $this->lastname;
    }
    function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }
    function getPhone()
    {
        return $this->phone;
    }
    function setPhone($phone)
    {
        $this->phone = $phone;
    }
    function getEmail()
    {
        return $this->email;
    }
    function setEmail($email)
    {
        $this->email = $email;
    }
    function getCity()
    {
        return $this->city;
    }
    function setCity($city)
    {
        $this->city = $city;
    }
    function getStreet()
    {
        return $this->street;
    }
    function setStreet($street)
    {
        $this->street = $street;
    }
    function getSecret()
    {
        return $this->secret;
    }
    function setSecret($psc)
    {
        $this->secret = $secret;
    }
    function getProfileinfo()
    {
        return $this->profileinfo;
    }
    function setProfileinfo($profileinfo)
    {
        $this->profileinfo = $profileinfo;
    }
	function plain( $str )
	{
		return htmlspecialchars( $str, ENT_QUOTES );
	}
    function selectInfoData() {
        include("../auth.php");
        $dbConnection = new dbConnection();
		$login_user = htmlspecialchars($_SESSION['username'],ENT_QUOTES);
        $result = mysqli_query($dbConnection->connectDB(),"SELECT * FROM userinfo 
                                                                LEFT JOIN userinfo ON users.id = userinfo.user_id 
                                                                WHERE users.username = '{$login_user}'");
        if (mysqli_num_rows($result) > 0) {
            while($row = mysqli_fetch_assoc($result)) {
                $username = '$row["username"]';
                $lastname = '$row["lastname"]';
                $phone = '$row["phone"]';
                $email = '$row["email"]';
                $city = '$row["city"]';
                $street = '$row["street"]';
                $secret = '$row["secret"]';
                $profileinfo = '$row["profileinfo"]';
                echo plain($username);
                echo plain($lastname);
                echo plain($phone);
                echo plain($email);
                echo plain($city);
                echo plain($street);
                echo plain($psc);
                echo plain($profileinfo);
            }
        } else {
            echo "0 výsledků";
        }
        return mysqli_close($dbConnection->connectDB());
    }
}
$selectInfoData = new usersInfoSelectServices;
echo $selectInfoData->selectInfoData();
?>