<?php
require ('../auth.php');
require ('../dbConnection/dbConnection.php');
class usersInfoServices {
    private $firstname;
    private $lastname;
    private $phone;
    private $email;
    private $city;
    private $street;
    private $secret;
	private $gender;
	private $relationship;
	private $image;
	private $facebook;
    private $profileinfo;
    function __construct() {
        $this->firstname;
        $this->lastname;
        $this->phone;
        $this->email;
        $this->city;
        $this->street;
        $this->secret;
		$this->gender;
        $this->relationship;
		$this->image;
		$this->facebook;
        $this->profileinfo;
    }
    function getFirstname()
    {
        return $this->firstname;
    }
    function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }
    function getLastname()
    {
        return $this->lastname;
    }
    function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }
    function getPhone()
    {
        return $this->phone;
    }
    function setPhone($phone)
    {
        $this->phone = $phone;
    }
    function getEmail()
    {
        return $this->email;
    }
    function setEmail($email)
    {
        $this->email = $email;
    }
    function getCity()
    {
        return $this->city;
    }
    function setCity($city)
    {
        $this->city = $city;
    }
    function getStreet()
    {
        return $this->street;
    }
    function setStreet($street)
    {
        $this->street = $street;
    }
    function getSecret()
    {
        return $this->secret;
    }
    function setSecret($secret)
    {
        $this->secret = $secret;
    }
	function getGender()
    {
        return $this->gender;
    }
    function setGender($gender)
    {
        $this->gender = $gender;
    }
	function getRelationship()
    {
        return $this->relationship;
    }
    function setRelationship($relationship)
    {
        $this->relationship = $relationship;
    }
	function getImage()
    {
        return $this->image;
    }
    function setImage($image)
    {
        $this->image = $image;
    }
	function getFacebook()
    {
        return $this->facebook;
    }
    function setFacebook($facebook)
    {
        $this->image = $facebook;
    }
    function getProfileinfo()
    {
        return $this->profileinfo;
    }
    function setProfileinfo($profileinfo)
    {
        $this->profileinfo = $profileinfo;
    }
    function selectAndUpdateData($firstname,$lastname,$phone,$email,$city,$street,$secret,$gender,$relationship,$image,$facebook,$profileinfo) {
		require ('../auth.php');
        $dbConnection = new dbConnection();
		$login_user = htmlspecialchars($_SESSION['username'],ENT_QUOTES);
        $query = mysqli_query($dbConnection->connectDB(),"SELECT userinfo.username FROM users 
                                                                LEFT JOIN userinfo ON users.id = userinfo.user_id 
                                                                WHERE users.username = '{$login_user}'");
        $count = mysqli_num_rows($query);
		
		if(!$login_user) {
			echo '<script>location.replace("index.php");</script>';
			die();
		}
        if ($count == 0) {
            $query = "INSERT INTO userinfo (username, lastname, phone, email, city, street, secret, gender, relationship, image, facebook, profileinfo) VALUES ('{$firstname}','{$lastname}','{$phone}','{$email}','{$city}','{$street}','{$secret}','{$gender}','{$relationship}','{$image}','{$facebook}','{$profileinfo}')";
            mysqli_query($dbConnection->connectDB(), $query);
            echo '<script>alert("Nastavení bylo změněno."); location.replace(document.referrer);</script>';
        }
        if($count > 0) {
            while ($row = mysqli_fetch_assoc($query)) {
				$queryUpdate = "UPDATE userinfo SET username = '{$firstname}', lastname = '{$lastname}', phone = '{$phone}', email = '{$email}', city = '{$city}', street = '{$street}', secret = '{$secret}', gender = '{$gender}', relationship = '{$relationship}', image = '{$image}', facebook = '{$facebook}',
				profileinfo = '{$profileinfo}' WHERE userinfo.username = '{$login_user}'";
                mysqli_query($dbConnection->connectDB(), $queryUpdate);
                echo '<script>alert("Nastavení bylo změněno."); location.replace(document.referrer);</script>';
            }
        }
        mysqli_close($dbConnection->connectDB());
    }
}
$username = htmlspecialchars($_SESSION["username"],ENT_QUOTES); 
$lastname = htmlspecialchars($_POST['lastname'],ENT_QUOTES);
$phone = htmlspecialchars($_POST['phone'],ENT_QUOTES);
$email = htmlspecialchars($_POST['email'],ENT_QUOTES);
$city = htmlspecialchars($_POST['city'],ENT_QUOTES);
$street = htmlspecialchars($_POST['street'],ENT_QUOTES);
$secret = htmlspecialchars($_POST['secret'],ENT_QUOTES);
$gender = htmlspecialchars($_POST['gender'],ENT_QUOTES);
$relationship = htmlspecialchars($_POST['relationship'],ENT_QUOTES);
$image = htmlspecialchars($_POST['image'],FILTER_VALIDATE_URL);
$facebook = htmlspecialchars($_POST['facebook'],FILTER_VALIDATE_URL);
$profileinfo = htmlspecialchars($_POST['profileinfo'],ENT_QUOTES);
$usersInfoServices = new UsersInfoServices();
$usersInfoServices->selectAndUpdateData($username, $lastname, $phone, $email, $city,
                                        $street, $secret, $gender, $relationship, $image, $facebook, $profileinfo);
?>

