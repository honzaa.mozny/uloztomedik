<style>
    body:not(.page-id-833) .grecaptcha-badge {
      z-index: 99999;
    }
</style>

<script src="https://www.google.com/recaptcha/api.js?render=6LfwghkbAAAAAM3GoyErvbC1M9peW-zvkThvSXY_"></script>
<script>
grecaptcha.ready(function() {
    grecaptcha.execute('6LfwghkbAAAAAM3GoyErvbC1M9peW-zvkThvSXY_', {action: 'homepage'}).then(function(token) {
    });
});
</script>

<?php
	function plain( $str )
		{
			return htmlspecialchars( $str, ENT_QUOTES );
		}
		function word_limiter($str, $limit = 100, $end_char = '&#8230;')
		{
			if (trim($str) === '') {
				return $str;
			}
			preg_match('/^\s*+(?:\S++\s*+){1,'.(int) $limit.'}/', $str, $matches);
			if (strlen($str) === strlen($matches[0])) {
				$end_char = '';
			}
			return rtrim($matches[0]).$end_char;
		}
	require('db/db.php');
	session_start();
    if (isset($_POST['username'])){
		$username = stripslashes($_REQUEST['username']);
		$username = mysqli_real_escape_string($con,$username);
		$password = stripslashes($_REQUEST['password']);
		$password = mysqli_real_escape_string($con,$password);
		$username = plain($username);
		$password = plain($password);
    $query = "SELECT * FROM `users` WHERE username='$username' and password='".md5($password)."'";
		$result = mysqli_query($con,$query) or die(mysqli_errno());
		$rows = mysqli_num_rows($result);
    if($rows == 1) {
        $_SESSION['username'] = $username;
        header("Location: views/profile/profileTemplate.php");
    } else {
		  echo "
		  <!DOCTYPE html>
          <html lang='en'>
            <head>
              <meta charset='utf-8'>
              <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
              <title>Důležité medicínské materiály - Vše na jednm místě!</title>
              <meta name='description' content='Medicínské materiály, ebooky, epub, pdf'>
              <meta name='author' content='Jan Možný'>
              <link rel='stylesheet' href='assets/css/bootstrap/bootstrap.min.css'>
              <link rel='icon' href='favicon.ico'>
            </head>
            <body>
              <div class=''>
                <section class='py-5 bg-light'>
                  <div class='container text-center'>
                    <a class'd-inline-block mb-5' href='#'></a>
                    <div class='row mb-4'>
                      <div class='col-12 col-md-8 col-lg-5 mx-auto'>
                        <div class='p-4 shadow-sm rounded bg-white'>
							<h2 class='mb-4 fw-light'>Přihlášení se nepodařilo</h2>
							<a href='login.php'>Zkusit znovu</a>
                        </div>
                      </div>
                    </div>
                    <p class='text-muted'><a class='text-muted' href='https://textinzerce.cz/register.php'>Registrovat se</a></p>
                  </div>
                </section>
              </div>
              <script src='js/bootstrap/bootstrap.bundle.min.js'></script>
              <script src='js/main.js'></script>
            </body>
          </html>";
		}
      } else {
          ?>
          <!DOCTYPE html>
          <html lang="en">
            <head>
              <meta charset="utf-8">
              <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
              <title>Důležité medicínské materiály - Vše na jednm místě!</title>
              <meta name='description' content='Medicínské materiály, ebooky, epub, pdf'>
              <meta name='author' content='Jan Možný'>
              <link rel="stylesheet" href="assets/css/bootstrap/bootstrap.min.css">
              <link rel="icon" href="favicon.ico">
            </head>
            <body>
              <div class="">
                <section class="py-5 bg-light">
                  <div class="container text-center">
                    <a class="d-inline-block mb-5" href="#"></a>
                    <div class="row mb-4">
                      <div class="col-12 col-md-8 col-lg-5 mx-auto">
                        <div class="p-4 shadow-sm rounded bg-white">
                          <span class="text-muted">Přihlásit se</span>
                          <form action="" method="POST">
                            <h2 class="mb-4 fw-light">Připoj se do <a style="text-decoration: none; color: rgba(56,44,221);" href="https://uloztomedik.cz/">Uloztomedik.cz</a></h2>
                            <div class="mb-3 input-group">
                              <input class="form-control" type="text" name="username" placeholder="Uživatelské jméno" required>
                              <span class="input-group-text">
                                <svg width="20" height="20" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 15v2m-6 4h12a2 2 0 002-2v-6a2 2 0 00-2-2H6a2 2 0 00-2 2v6a2 2 0 002 2zm10-10V7a4 4 0 00-8 0v4h8z"></path>
                                </svg>
                              </span>
                            </div>
                            <div class="mb-3 input-group">
                              <input class="form-control" type="password" name="password" placeholder="Heslo" required>
                              <div class="input-group-text">
                                <svg width="20" height="20" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 15v2m-6 4h12a2 2 0 002-2v-6a2 2 0 00-2-2H6a2 2 0 00-2 2v6a2 2 0 002 2zm10-10V7a4 4 0 00-8 0v4h8z"></path>
                                </svg>
                              </div>
                            </div>
                            <button style="cursor:pointer; background-color: rgba(56,44,221);" name="submit" type="submit" class="btn btn-primary w-100">Přihlásit se</button>
                          </form>
                        </div>
                      </div>
                    </div>
                    <p class="text-muted"><a class="text-muted" href="/register.php">Registrovat se</a></p>
                  </div>
                </section>
              <script src="assets/js/bootstrap/bootstrap.bundle.min.js"></script>
              <script src="assets/js/main.js"></script>
            </body>
          </html>
<?php } ?>
